import uuid
from typing import Dict, Optional

from app.services.client import Client
from app.services.bank_account_manager import BankAccountManager


class Bank:
    def __init__(self):
        self.clients: Dict[uuid.UUID, Client] = {}

    def add_client(self, client: Client) -> None:
        self.clients[client.id] = client

    def get_all_bank_account_statuses(self, account_manager: BankAccountManager) -> str:
        statuses: str = "".join(
            self.__get_account_status_for_client(client, account_manager)
            for client in self.clients.values()
        )
        return statuses

    @staticmethod
    def __get_account_status_for_client(
        client: Client, account_manager: BankAccountManager
    ) -> str:
        account_statuses: str = "".join(
            f"Account ID: {account_id}, Soldes: {account.balance}, Limite: {account.overdraft_limit}\n"
            for account_id in client.bank_account_ids
            if (account := account_manager.find_account(account_id))
        )
        return account_statuses

    def __repr__(self) -> str:
        return f"Bank({self.clients})"
