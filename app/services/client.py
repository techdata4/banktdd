import uuid
from typing import Dict, Union

from app.services.bank_account import BankAccount


class Client:
    def __init__(
        self, last_name: str, first_name: str, address_ids: list, client_id: uuid.UUID
    ):
        self.id = client_id
        self._last_name = last_name
        self._first_name = first_name
        self._address_ids = address_ids
        self._bank_account_ids = []

    def add_account(self, account_id: uuid.UUID):
        self._bank_account_ids.append(account_id)

    @property
    def last_name(self) -> str:
        return self._last_name

    @property
    def first_name(self) -> str:
        return self._first_name

    @property
    def address_ids(self) -> list:
        return self._address_ids

    @property
    def bank_account_ids(self) -> list:
        return self._bank_account_ids
