import uuid
from typing import Optional, Dict
from app.services.bank_account import BankAccount


class BankAccountManager:
    def __init__(self):
        self.accounts: Dict[uuid.UUID, BankAccount] = {}

    def create_account(self, overdraft_limit: float, balance: float) -> BankAccount:
        account_id = uuid.uuid4()
        account = BankAccount(overdraft_limit, balance, account_id)
        self.accounts[account_id] = account
        return account

    def find_account(self, account_id: uuid.UUID) -> Optional[BankAccount]:
        return self.accounts.get(account_id)
