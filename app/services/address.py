class Address:
    """
    Initializes an instance of the Address class.

    :param street_name: The name of the street.
    :param postal_code: The postal code of the address.
    :param floor: The floor of the address (optional).
    """

    def __init__(self, street_name: str, postal_code: str, floor: str = None):
        self._street_name = street_name
        self._postal_code = postal_code
        self._floor = floor
