import uuid


class BankAccount:
    def __init__(self, overdraft_limit: float, balance: float, client_id: uuid.UUID):
        if balance < 0 or overdraft_limit < 0:
            raise ValueError("Balance and overdraft limit must be non-negative.")
        self.id = uuid.uuid4()
        self.__balance = balance
        self.__overdraft_limit = overdraft_limit
        self.__client_id = client_id

    @property
    def balance(self) -> float:
        return self.__balance

    @property
    def overdraft_limit(self) -> float:
        return self.__overdraft_limit

    def deposit(self, amount: float) -> None:
        if amount < 0:
            raise ValueError("Deposit amount cannot be negative.")
        self.__balance += amount

    def withdraw(self, amount: float) -> None:
        if amount < 0:
            raise ValueError("Withdrawal amount cannot be negative.")
        if not self.is_within_overdraft_limit(amount):
            raise ValueError(
                f"Insufficient funds for this operation. Current balance: {self.__balance}, "
                f"Attempted withdrawal: {amount}"
            )
        self.__balance -= amount

    def is_within_overdraft_limit(self, amount: float) -> bool:
        return self.__balance - amount >= -self.__overdraft_limit
