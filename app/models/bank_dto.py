from pydantic import BaseModel, Field, field


class BankAccountDTO(BaseModel):
    overdraft_limit: float = Field(
        ..., description="The overdraft limit for the bank account"
    )
    balance: float = Field(..., description="The solde of the bank account")

    @overdraft_limit.validator
    def validate_overdraft_limit(cls, value):
        if value < 0:
            raise ValueError("Overdraft limit must be positive")
        return value

    @balance.validator
    def validate_balance(cls, value):
        if value < 0:
            raise ValueError("Balance must be positive")
        return value
