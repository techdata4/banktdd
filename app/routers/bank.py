from typing import Union

from app.services import bank_account

ACCOUNT_STATUS_FORMAT = "Compte {}: Balance: {}, de limit: {}\n"


class Bank:
    """
    This class represents a bank which manages multiple bank accounts.
    """

    def __init__(self):
        self.accounts = {}
        self.next_id = 1

    def create_account(self, authorized_overdraft: float) -> BankAccount:
        new_account = BankAccount
        self.accounts[self.next_id] = new_account
        self.next_id += 1
        return new_account

    def get_account_by_id(self, account_id: int) -> Union[BankAccount, None]:
        return self.accounts.get(account_id)


class BankReport:
    """
    This class generates reports for a bank.
    """

    def __init__(self, bank: Bank):
        self.bank = bank

    def _account_status(self, id: int, account: BankAccount) -> str:
        return ACCOUNT_STATUS_FORMAT.format(id, account.balance, account.limit)

    def generate_accounts_status(self) -> str:
        return "".join(
            self._account_status(id, account)
            for id, account in self.bank.accounts.items()
        )
