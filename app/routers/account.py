class InsufficientFundsException(Exception):
    def __init__(self, message="Insufficient funds"):
        super().__init__(message)


class BankAccount:
    def __init__(self, overdraft_limit: float, balance: float, Person):
        self.limit = None
        self.__balance = 0.0
        self.__overdraft_limit = overdraft_limit
        self.__balance = balance
        self.__person = Person

    @property
    def balance(self) -> float:
        return self.__balance

    @property
    def overdraft_limit(self) -> float:
        return self.__overdraft_limit

    @overdraft_limit.setter
    def overdraft_limit(self, new_overdraft_limit: float):
        if new_overdraft_limit < 0:
            raise ValueError("Overdraft limit cannot be negative.")
        self.__overdraft_limit = new_overdraft_limit

    def deposit(self, amount: float) -> None:
        self.__balance += amount

    def withdraw(self, amount: float) -> None:
        new_balance = self.__balance - amount
        if new_balance < -self.__overdraft_limit:
            raise InsufficientFundsException(
                f"Insufficient funds for this operation. Balance: {self.__balance}"
                f", Overdraft limit: {self.__overdraft_limit},"
                f" Amount to withdraw: {amount}")
        self.__balance = new_balance
