import pytest
from app.compte import BankAccount, InsufficientFundsException


def test_bank_account_initialization():
    account = BankAccount(100.0)
    assert account.balance == 0.0
    assert account.overdraft_limit == 100.0


def test_bank_account_deposit():
    account = BankAccount(100.0)
    account.deposit(50.0)
    assert account.balance == 50.0


def test_bank_account_set_overdraft_limit():
    account = BankAccount(100.0)
    account.overdraft_limit = 200.0
    assert account.overdraft_limit == 200.0


def test_overdraft_limit_value_error():
    account = BankAccount(100.0)
    with pytest.raises(ValueError):
        account.overdraft_limit = -100.0


def test_bank_account_withdrawal():
    account = BankAccount(100.0)
    account.deposit(50.0)
    account.withdraw(20.0)
    assert account.balance == 30.0


def test_bank_account_insufficient_funds():
    account = BankAccount(100.0)
    account.deposit(50.0)
    with pytest.raises(InsufficientFundsException):
        account.withdraw(200.0)
