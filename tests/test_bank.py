import unittest
from unittest.mock import Mock
import uuid
from app.services.bank_account_manager import BankAccountManager
from app.services.bank import Bank
import factory


class ClientFactory(factory.Factory):
    class Meta:
        model = Mock

    id = factory.LazyFunction(uuid.uuid4)
    bank_account_ids = factory.List(
        [factory.LazyFunction(uuid.uuid4) for _ in range(3)]
    )


class TestBank(unittest.TestCase):
    def setUp(self):
        self.bank = Bank()
        self.clients = [ClientFactory() for _ in range(3)]
        for client in self.clients:
            self.bank.add_client(client)

    def test_init(self):
        self.assertEqual(
            self.bank.clients, {client.id: client for client in self.clients}
        )

    def test_add_client(self):
        client = ClientFactory()
        self.bank.add_client(client)
        self.assertIn(client.id, self.bank.clients)

    def test_get_all_bank_account_statuses(self):
        account_manager_mock = Mock(spec=BankAccountManager)
        for client in self.clients:
            self.bank.add_client(client)
        self.bank.get_all_bank_account_statuses(account_manager_mock)
        account_manager_mock.find_account.assert_called()

    def test_repr(self):
        client = ClientFactory()
        self.bank.add_client(client)
        self.assertEqual(self.bank.__repr__(), f"Bank({self.bank.clients})")
        print(self.bank.__repr__())


if __name__ == "__main__":
    unittest.main()
