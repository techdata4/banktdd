import unittest
import uuid

from app.services.client import Client


class TestClient(unittest.TestCase):
    CLIENT_ID = uuid.uuid4()

    def setUp(self):
        self.client = Client("Doe", "John", [], self.CLIENT_ID)

    def test_add_multiple_accounts(self):
        account_ids = [uuid.uuid4() for _ in range(3)]
        for account_id in account_ids:
            self.client.add_account(account_id)
        self.assertEqual(len(self.client.bank_account_ids), 3)
        for account_id in account_ids:
            self.assertIn(account_id, self.client.bank_account_ids)


if __name__ == "__main__":
    unittest.main()
