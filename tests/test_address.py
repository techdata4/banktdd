import unittest

from app.services.client import Address


class TestAddress(unittest.TestCase):
    def setUp(self):
        self.address = Address(
            street_name="123 Rue Principale", postal_code="75000", floor="3"
        )

    def test_address_initialization(self):
        self.assertEqual(self.address._street_name, "123 Rue Principale")
        self.assertEqual(self.address._postal_code, "75000")
        self.assertEqual(self.address._floor, "3")

    # Ajoutez ici des tests supplémentaires pour les cas spécifiques de Address
    def test_address_street_name_setter(self):
        self.address.street_name = "456 Rue Secondaire"
        self.assertEqual(self.address.street_name, "456 Rue Secondaire")

    def test_address_postal_code_setter(self):
        self.address.postal_code = "75001"
        self.assertEqual(self.address.postal_code, "75001")
