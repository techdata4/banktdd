import unittest
import uuid
from app.services.bank_account import BankAccount


class TestBankAccount(unittest.TestCase):
    INITIAL_BALANCE = 1000.0
    OVERDRAFT_LIMIT = 500.0
    DEPOSIT_AMOUNT = 200.0
    WITHDRAW_AMOUNT = 300.0
    EXCESSIVE_WITHDRAW_AMOUNT = (
        INITIAL_BALANCE + OVERDRAFT_LIMIT + 100.0
    )  # Exceeds overdraft limit
    NEGATIVE_AMOUNT = -100.0

    def setUp(self):
        self.client_id = uuid.uuid4()
        self.account = BankAccount(
            self.OVERDRAFT_LIMIT, self.INITIAL_BALANCE, self.client_id
        )

    def test_initialization(self):
        self.assertEqual(self.account.balance, self.INITIAL_BALANCE)
        self.assertEqual(self.account.overdraft_limit, self.OVERDRAFT_LIMIT)

    def test_deposit_positive_amount(self):
        self.account.deposit(self.DEPOSIT_AMOUNT)
        self.assertEqual(
            self.account.balance, self.INITIAL_BALANCE + self.DEPOSIT_AMOUNT
        )

    def test_deposit_negative_amount(self):
        with self.assertRaises(ValueError):
            self.account.deposit(self.NEGATIVE_AMOUNT)

    def test_withdraw_within_limit(self):
        self.account.withdraw(self.WITHDRAW_AMOUNT)
        self.assertEqual(
            self.account.balance, self.INITIAL_BALANCE - self.WITHDRAW_AMOUNT
        )

    def test_withdraw_exceeds_balance_within_overdraft(self):
        withdraw_amount = self.INITIAL_BALANCE + (self.OVERDRAFT_LIMIT / 2)
        self.account.withdraw(withdraw_amount)
        self.assertEqual(self.account.balance, self.INITIAL_BALANCE - withdraw_amount)

    def test_withdraw_exceeds_overdraft_limit(self):
        with self.assertRaises(ValueError):
            self.account.withdraw(self.EXCESSIVE_WITHDRAW_AMOUNT)

    def test_negative_withdrawal(self):
        with self.assertRaises(ValueError):
            self.account.withdraw(self.NEGATIVE_AMOUNT)

    def test_initialization_with_negative_balance(self):
        with self.assertRaises(ValueError):
            BankAccount(self.OVERDRAFT_LIMIT, self.NEGATIVE_AMOUNT, self.client_id)

    def test_initialization_with_negative_overdraft_limit(self):
        with self.assertRaises(ValueError):
            BankAccount(self.NEGATIVE_AMOUNT, self.INITIAL_BALANCE, self.client_id)


if __name__ == "__main__":
    unittest.main()
